const ESLintPlugin = require('eslint-webpack-plugin');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const Dotenv = require('dotenv-webpack');
const webpack = require('webpack');
const WebpackBuildNotifierPlugin = require('webpack-build-notifier');

const packageJson = require('./package.json');

const plugins = [
  new HtmlWebpackPlugin({
    title: 'My App',
    template: './public/index.html',
  }),
  new webpack.DefinePlugin({}),
  new Dotenv({}),
  new MiniCssExtractPlugin(),
  new ESLintPlugin({ extensions: ['js', 'jsx', 'ts', 'tsx'] }),
  new WebpackBuildNotifierPlugin({
    title: packageJson.name,
    suppressCompileStart: true,
  }),
];

if (process.env.ANALYZE) {
  plugins.push(new BundleAnalyzerPlugin());
}

module.exports = env => {
  console.log('Production: ', env.production);
  console.log('Dev: ', env.development);

  return {
    entry: './src/index.ts',
    mode: env.production ? 'production' : 'development',
    devtool: 'source-map',
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist'),
      clean: true,
      publicPath: '/',
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx', '.scss'],
    },
    module: {
      rules: [
        {
          test: /\.scss$/i,
          exclude: /\.module\.scss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: {
                  mode: 'icss',
                },
              },
            },
            'sass-loader',
          ],
        },
        {
          test: /\.module\.scss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                modules: {
                  exportLocalsConvention: 'camelCaseOnly',
                  localIdentName: '[local]',
                },
              },
            },
            'sass-loader',
          ],
        },
        {
          test: /\.svg$/,
          use: ['@svgr/webpack'],
        },
        {
          test: /\.(png|jpg|jpeg|gif)$/i,
          type: 'asset/resource',
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/i,
          type: 'asset/resource',
        },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ],
    },
    plugins,
    devServer: {
      static: {
        directory: path.join(__dirname, 'dist'),
      },
      compress: false,
      port: 3000,
      hot: true,
      historyApiFallback: true,
    },
  };
};
