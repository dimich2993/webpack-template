import { render } from 'react-dom';
import App from './app/App';
import Modal from 'react-modal';
import './app/styles/index.scss';

const root = document.getElementById('root');

if (root) {
  Modal.setAppElement(root);
  render(App, root);
} else {
  throw new Error('Root element could not be found.');
}
