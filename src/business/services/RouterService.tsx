import { injectable } from 'inversify';
import { createBrowserHistory, History } from 'history';

@injectable()
export class RouterService {
  readonly history: History;

  constructor() {
    this.history = createBrowserHistory();
  }
}
