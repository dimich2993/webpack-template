import { injectable } from 'inversify';

export interface EnvVars {
  SERVICE_URL: string;
}

@injectable()
export class EnvVarsService implements EnvVars {
  readonly SERVICE_URL = process.env.REACT_APP_SERVICE_URL as string;
}
