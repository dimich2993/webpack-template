import { injectable } from 'inversify';
import { AuthStorageService } from './AuthStorageService';
import { routes } from '../../app/router/routes';
import { RouterService } from './RouterService';

@injectable()
export class AuthService {
  constructor(
    private authStorage: AuthStorageService,
    private router: RouterService,
  ) {}

  logout = () => {
    this.authStorage.removeToken();
    this.router.history.push(routes.form.getPath());
  };
}
