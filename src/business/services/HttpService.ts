import axios, { AxiosRequestHeaders, Method } from 'axios';
import qs from 'qs';
import { Axios } from 'axios';
import { injectable } from 'inversify';

import { EnvVarsService } from './EnvVarsService';

@injectable()
export class HttpService {
  public axios: Axios;

  get interceptors() {
    return this.axios.interceptors;
  }

  constructor(private env: EnvVarsService) {
    this.axios = axios.create({
      params: {},
      paramsSerializer: params =>
        qs.stringify(params, { arrayFormat: 'repeat' }),
    });
  }

  request<T>(
    method: Method,
    url: string,
    headers: AxiosRequestHeaders,
    query?: never,
    body?: never,
  ) {
    return this.axios.request<T>({
      baseURL: this.env.SERVICE_URL,
      method,
      url,
      params: query,
      data: body,
      withCredentials: true,
      headers,
    });
  }
}
