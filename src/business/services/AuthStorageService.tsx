import { injectable } from 'inversify';

@injectable()
export class AuthStorageService {
  readonly tokenKey: string = 'token';

  // TODO: implement the logic to check if token is really valid
  get isAuth(): boolean {
    return !!localStorage.getItem(this.tokenKey);
  }

  getToken(): string | null {
    return localStorage.getItem(this.tokenKey);
  }

  setToken(token: string): void {
    localStorage.setItem(this.tokenKey, token);
  }

  removeToken(): void {
    localStorage.removeItem(this.tokenKey);
  }
}
