import i18next, { i18n, Resource } from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { injectable } from 'inversify';
import { initReactI18next } from 'react-i18next';

@injectable()
export class I18nService {
  private _i18n: i18n;

  constructor() {
    this.initializeI18n(['ru'], {
      ru: { translations: { fetch: 'стянуть' } },
    });
  }

  initializeI18n = (fallbackLng: string[], resources: Resource) => {
    this._i18n = i18next.createInstance();

    this._i18n
      .use(LanguageDetector)
      .use(initReactI18next)
      .init({
        fallbackLng,
        nsSeparator: false,
        keySeparator: false,
        debug: false,
        interpolation: {
          escapeValue: false,
        },
        detection: {
          order: ['localStorage'],
          lookupLocalStorage: 'lang',
        },
        resources,
        ns: ['translations'],
        defaultNS: 'translations',
      });
  };
}
