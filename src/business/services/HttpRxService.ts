import { Method, AxiosRequestHeaders } from 'axios';
import { injectable } from 'inversify';
import { Subject, from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { HttpService } from './HttpService';

export interface RequestDTO {
  query?: never;
  body?: never;
}

export interface HttpRequest<D extends RequestDTO> {
  method: Method;
  headers: AxiosRequestHeaders;
  url: string;
  data: D;
}

export interface MsgCreatorFn<RQ> {
  (rq: RQ): HttpRequest<RQ>;
}

export interface HttpMessagePipe<RQ, RS> {
  req$: Subject<RQ>;
  res$: Subject<RS>;
  err$: Subject<unknown>;
}

@injectable()
export class HttpRxService {
  constructor(private http: HttpService) {}

  public pipe<RQ, RS>(msgCreator: MsgCreatorFn<RQ>): HttpMessagePipe<RQ, RS> {
    const req$ = new Subject<RQ>();
    const res$ = new Subject<RS>();
    const err$ = new Subject<unknown>();

    req$
      .pipe(map(msgCreator), this.switchMapToHttp<RQ, RS>())
      .subscribe({ next: res => res$.next(res), error: err => err$.next(err) });

    return { req$, res$, err$ };
  }

  private switchMapToHttp = <RQ extends RequestDTO, RS>() =>
    switchMap(
      ({ method, url, headers, data: { query, body } }: HttpRequest<RQ>) =>
        from(this.http.request<RS>(method, url, headers, query, body)).pipe(
          map(res => res.data),
        ),
    );

  public request =
    (method: Method, headers: AxiosRequestHeaders = {}) =>
    <RQ, RS>(url: string) =>
      this.pipe<RQ, RS>((data: RQ) => ({
        method,
        url,
        data,
        headers,
      }));

  public get = this.request('GET');
  public post = this.request('POST');
  public postFormData = this.request('POST', {
    'Content-Type': 'multipart/form-data',
  });
  public put = this.request('PUT');
  public delete = this.request('DELETE');
}
