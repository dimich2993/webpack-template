import { from, Subject } from 'rxjs';
import { injectable } from 'inversify';

@injectable()
export class CatRepo {
  readonly randomCatPhoto$ = new Subject<{
    id: string;
    url: string;
  }>();

  fetchRandomCatPhoto = () => {
    from(
      fetch('https://thatcopy.pw/catapi/rest/').then(res => res.json()),
    ).subscribe(this.randomCatPhoto$);
  };
}
