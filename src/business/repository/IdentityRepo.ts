import { filter } from 'rxjs';
import { injectable } from 'inversify';
import { AuthStorageService } from '../services/AuthStorageService';
import { HttpRxService } from '../services/HttpRxService';

export interface LoginResponse {
  success: boolean;
  error?: unknown;
}

@injectable()
export class IdentityRepo {
  public login = this.http.get<never, LoginResponse>(
    'https://thatcopy.pw/catapi/rest/',
  );

  constructor(private auth: AuthStorageService, private http: HttpRxService) {
    this.login.res$
      .pipe(filter((response: LoginResponse) => response.success))
      .subscribe(() => {
        this.auth.setToken('token');
      });

    this.login.err$.subscribe((err: Error) => {
      console.log('LOGIN ERROR: ', err);
    });
  }
}
