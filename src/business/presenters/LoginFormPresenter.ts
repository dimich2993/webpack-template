import isUndefined from 'lodash/isUndefined';
import * as yup from 'yup';
import { routes } from '../../app/router/routes';
import { injectable } from 'inversify';
import { map, filter, delay } from 'rxjs/operators';
import { IdentityRepo } from '../repository/IdentityRepo';
import { RouterService } from '../services/RouterService';

@injectable()
export class LoginFormPresenter {
  validationRules = yup.object().shape({
    login: yup.string().min(3, 'Min 3 characters').required('Required'),
    password: yup.string().required('Required'),
    rememberMe: yup.bool(),
  });

  constructor(
    public identityRepo: IdentityRepo,
    private router: RouterService,
  ) {
    this.isSuccess$
      .pipe(
        filter(isSuccess => isSuccess),
        delay(1000),
      )
      .subscribe({
        next: () => this.router.history.push(routes.private.getPath()),
      });

    this.identityRepo.login.res$
      .pipe(filter(response => !isUndefined(response.error)))
      .subscribe({
        next: ({ error }) => console.error(error),
      });
  }

  public isSuccess$ = this.identityRepo.login.res$.pipe(
    map(res => res.success),
  );

  // important to have here an arrow function
  public handleSubmit = (values: never) => {
    this.identityRepo.login.req$.next(values);
  };
}
