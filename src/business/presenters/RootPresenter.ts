import { injectable } from 'inversify';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import { AuthStorageService } from '../services/AuthStorageService';
import { HttpService } from '../services/HttpService';

@injectable()
export class RootPresenter {
  constructor(
    private http: HttpService,
    private authStorage: AuthStorageService,
  ) {
    this.http.interceptors.request.use(
      request => {
        const token = this.authStorage.getToken();

        if (!isNull(token) && !isUndefined(request.headers)) {
          request.headers.Authorization = token;
        }

        return request;
      },
      err => {
        console.log('Interceptor req error: ', err);
      },
    );

    this.http.interceptors.response.use(
      response => {
        console.log(response);
        return response;
      },
      err => {
        console.log('Interceptor res error: ', err);
      },
    );
  }
}
