import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router-dom';

const HomePage: FC<RouteComponentProps> = () => {
  return <div>Homepage</div>;
};

export default HomePage;
