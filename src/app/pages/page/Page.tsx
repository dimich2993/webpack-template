import React, { FC } from 'react';
import { useInjection } from 'inversify-react';
import { RouteComponentProps } from 'react-router-dom';
import { useObservableState } from 'observable-hooks';
import { useTranslation } from 'react-i18next';
import { CatRepo } from '../../../business/repository/CatRepo';
import styles from './Page.module.scss';

const Page: FC<RouteComponentProps> = () => {
  const s = useInjection(CatRepo);
  const photo = useObservableState(s.randomCatPhoto$);
  const { t } = useTranslation();

  return (
    <div>
      {photo && <img key={photo.id} src={photo.url} />}
      <button className={styles.cFetchPhoto} onClick={s.fetchRandomCatPhoto}>
        {t('fetch')}
      </button>
    </div>
  );
};

export default Page;
