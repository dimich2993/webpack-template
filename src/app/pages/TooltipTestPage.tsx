import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';

const TooltipTestPage: FC<RouteComponentProps> = () => {
  return (
    <div>
      <p data-tip="hello world">Tooltip</p>
      <ReactTooltip />
    </div>
  );
};

export default TooltipTestPage;
