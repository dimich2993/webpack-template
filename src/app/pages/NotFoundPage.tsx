import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router-dom';

const NotFoundPage: FC<RouteComponentProps> = () => {
  return <div>404</div>;
};

export default NotFoundPage;
