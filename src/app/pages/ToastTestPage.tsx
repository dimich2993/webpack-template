import React, { FC } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { RouteComponentProps } from 'react-router-dom';

const ToastTestPage: FC<RouteComponentProps> = () => {
  const notify = () => toast('Wow so easy!');

  return (
    <div>
      <button onClick={notify}>Notify!</button>
      <ToastContainer />
    </div>
  );
};

export default ToastTestPage;
