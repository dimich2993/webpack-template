import { useInjection } from 'inversify-react';
import React, { FC } from 'react';
import { RouteComponentProps, useHistory } from 'react-router-dom';
import { EnvVarsService } from '../../business/services/EnvVarsService';
import { AuthService } from '../../business/services/AuthService';

const PrivatePage: FC<RouteComponentProps> = () => {
  const history = useHistory();
  const env = useInjection(EnvVarsService);
  const auth = useInjection(AuthService);

  return (
    <div>
      Here is a private information {env.SERVICE_URL}
      <button onClick={auth.logout}>Logout</button>
      <button onClick={history.goBack}>Go back</button>
    </div>
  );
};

export default PrivatePage;
