import React, { FC } from 'react';
import get from 'lodash/get';
import { useInjection } from 'inversify-react';
import { RouteComponentProps } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useObservableState } from 'observable-hooks';
import { LoginFormPresenter } from '../../business/presenters/LoginFormPresenter';
import { useHttpLoadingSuspense } from '../hooks/useLoadingSuspense';

const FormPage: FC<RouteComponentProps> = () => {
  const loginFormPresenter = useInjection(LoginFormPresenter);

  const isSuccess = useObservableState(loginFormPresenter.isSuccess$);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(loginFormPresenter.validationRules),
    mode: 'all',
  });

  const ui = useHttpLoadingSuspense(
    loginFormPresenter.identityRepo.login,
    () => <div>Login via</div>,
    res => <div>Success: {res.success}</div>,
    () => <div>Initial</div>,
  );

  return (
    <div>
      {isSuccess && isSuccess.toString()}
      <form noValidate onSubmit={handleSubmit(loginFormPresenter.handleSubmit)}>
        <label>Login: </label>
        <input type="text" {...register('login')} />
        <small>{get(errors, 'login.message')}</small>
        <br />
        <label>Password: </label>
        <input type="text" {...register('password')} />
        <small>{get(errors, 'password.message')}</small>
        <br />
        <label>
          Remember me <input type="checkbox" {...register('rememberMe')} />
        </label>
        <br />
        <button type="submit">Submit</button>
        {ui}
      </form>
    </div>
  );
};

export default FormPage;
