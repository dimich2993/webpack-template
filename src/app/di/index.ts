import 'reflect-metadata';
import { Container } from 'inversify';

import { LoginFormPresenter } from '../../business/presenters/LoginFormPresenter';
import { CatRepo } from '../../business/repository/CatRepo';
import { IdentityRepo } from '../../business/repository/IdentityRepo';
import { AuthStorageService } from '../../business/services/AuthStorageService';
import { AuthService } from '../../business/services/AuthService';
import { HttpService } from '../../business/services/HttpService';
import { HttpRxService } from './../../business/services/HttpRxService';
import { I18nService } from '../../business/services/I18nService';
import { EnvVarsService } from '../../business/services/EnvVarsService';
import { RootPresenter } from '../../business/presenters/RootPresenter';
import { RouterService } from '../../business/services/RouterService';

export const container = new Container();

// TODO: services should be application level based and business layer should know only about interfaces
container.bind(EnvVarsService).toSelf().inSingletonScope();
container.bind(HttpService).toSelf().inSingletonScope();
container.bind(HttpRxService).toSelf().inSingletonScope();
container.bind(I18nService).toSelf().inSingletonScope();
container.bind(AuthService).toSelf().inSingletonScope();
container.bind(AuthStorageService).toSelf().inSingletonScope();
container.bind(RouterService).toSelf();

container.bind(CatRepo).toSelf().inSingletonScope();
container.bind(IdentityRepo).toSelf().inSingletonScope();
container.bind(LoginFormPresenter).toSelf().inSingletonScope();
container.bind(RootPresenter).toSelf().inSingletonScope();
