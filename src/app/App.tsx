import React from 'react';
import { Provider } from 'inversify-react';
import { container } from './di';

import RootRouter from './router/RootRouter';

const App = () => {
  return (
    <Provider container={container}>
      <RootRouter />
    </Provider>
  );
};

export default <App />;
