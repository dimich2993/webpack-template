import { FC } from 'react';
import { useObservable, useObservableState } from 'observable-hooks';
import { delay, map, startWith, switchMapTo } from 'rxjs';
import { HttpMessagePipe } from '../../business/services/HttpRxService';

export const useHttpLoadingSuspense = <RQ, RS>(
  { req$, res$ }: HttpMessagePipe<RQ, RS>,
  loading: FC<RQ>,
  success: FC<RS>,
  initial: FC,
) => {
  const obs = useObservable(() =>
    req$.pipe(
      switchMapTo(res$.pipe(delay(1000), map(success), startWith(loading))),
    ),
  );

  return useObservableState(obs, initial);
};
