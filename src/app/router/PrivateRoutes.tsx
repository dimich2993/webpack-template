import React from 'react';
import { useInjection } from 'inversify-react';
import { routes } from './routes';
import { PrivateRoute } from './PrivateRoute';
import { AuthStorageService } from '../../business/services/AuthStorageService';

export const PrivateRoutes = () => {
  const authStorage = useInjection(AuthStorageService);
  console.log(authStorage.isAuth);
  return (
    <PrivateRoute
      // TODO: set real logic of Auth
      isAvailable
      path={routes.private.pattern}
      component={routes.private.Component}
      redirectTo={routes.form.getPath()}
    />
  );
};
