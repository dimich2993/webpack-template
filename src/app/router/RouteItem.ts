import { FC } from 'react';
import { encode } from 'querystring';
import isEmpty from 'lodash/isEmpty';
import { RouteComponentProps } from 'react-router-dom';

export class RouteItem<T> {
  constructor(
    public pattern: string,
    readonly Component: FC<RouteComponentProps>,
    readonly getPath: (
      this: Omit<RouteItem<never>, 'getPath'>,
      query?: T,
    ) => string = RouteItem.createGetPath(),
  ) {}

  static createGetPath = () => {
    return function (this: Omit<RouteItem<never>, 'getPath'>) {
      return this.pattern;
    };
  };

  static generatePath = (
    pattern: string,
    params: Record<string, string> = {},
    query: Record<string, string> = {},
  ) => {
    let path = pattern;

    Object.keys(params).forEach((key: string) => {
      path = path.replace(`:${key}`, params[key]);
    });

    if (!isEmpty(query)) {
      path = `${path}?${encode(query)}`;
    }

    return path;
  };
}
