import React, { FC } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

export const PrivateRoute: FC<
  RouteProps<string> & { isAvailable: boolean; redirectTo: string }
> = ({ children, isAvailable, redirectTo, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAvailable ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: redirectTo,
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};
