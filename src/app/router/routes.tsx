import { RouteItem } from './RouteItem';
import HomePage from '../pages/HomePage';
import NotFoundPage from '../pages/NotFoundPage';
import Page from '../pages/page/Page';
import LoginPage from '../pages/LoginPage';
import PrivatePage from '../pages/PrivatePage';
import ModalTestPage from '../pages/ModalTestPage';
import ToastTestPage from '../pages/ToastTestPage';
import TooltipTestPage from '../pages/TooltipTestPage';

export const routes = {
  home: new RouteItem('/', HomePage),
  page: new RouteItem('/page/:id', Page, function (id: string) {
    return RouteItem.generatePath(this.pattern, { id });
  }),
  modal: new RouteItem('/modal', ModalTestPage),
  toast: new RouteItem('/toast', ToastTestPage),
  tooltip: new RouteItem('/tooltip', TooltipTestPage),
  form: new RouteItem('/form', LoginPage),
  private: new RouteItem('/private', PrivatePage),
  _404: new RouteItem('/404', NotFoundPage),
};
