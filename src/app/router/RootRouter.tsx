import React, { FC } from 'react';
import {
  Link,
  Redirect,
  Route,
  Router as BrowserRouter,
  Switch,
} from 'react-router-dom';
import { routes } from './routes';
import { PrivateRoutes } from './PrivateRoutes';
import { useInjection } from 'inversify-react';
import { RootPresenter } from '../../business/presenters/RootPresenter';
import { RouterService } from '../../business/services/RouterService';

const RootRouter: FC = () => {
  useInjection(RootPresenter);
  const history = useInjection(RouterService).history;

  return (
    <BrowserRouter history={history}>
      <Link to={routes.home.getPath()}>Home</Link>{' '}
      <Link to={routes.page.getPath('123')}>Page</Link>{' '}
      <Link to={routes.modal.getPath()}>Modal test</Link>{' '}
      <Link to={routes.toast.getPath()}>Toast test</Link>{' '}
      <Link to={routes.tooltip.getPath()}>Tooltip test</Link>{' '}
      <Link to={routes.form.getPath()}>Login</Link>
      <Switch>
        <Route
          exact
          path={routes.home.pattern}
          component={routes.home.Component}
        />
        <Route path={routes.page.pattern} component={routes.page.Component} />
        <Route path={routes.modal.pattern} component={routes.modal.Component} />
        <Route path={routes.toast.pattern} component={routes.toast.Component} />
        <Route
          path={routes.tooltip.pattern}
          component={routes.tooltip.Component}
        />
        <Route path={routes.form.pattern} component={routes.form.Component} />
        <Route path={routes._404.pattern} component={routes._404.Component} />
        <PrivateRoutes />
        <Redirect
          to={{
            pathname: routes._404.pattern,
            state: { referrer: location.pathname },
          }}
        />
      </Switch>
    </BrowserRouter>
  );
};

export default RootRouter;
