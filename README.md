## React + Angular = Rectangular

### Features:

- typescript
- typed scss modules
- eslint with prettier plugin
- webpack bundle analyzer
- webpack eslint guard
- dotenv files support
- commit linting
- commit template
- react support
- webpack build desktop notification
- svg, image loaders
- i18n
- spa router
  - https://stackoverflow.com/questions/45598779/react-router-browserrouter-leads-to-404-not-found-nginx-error-when-going-to
- forms
- http service
- env config service
- router history service
- i18n service support
- process axios errors
- http service with auth support

### TODO:

- loader service support
- toastify service support
- modal dialog service support
- tooltip service support
- contextual menu service support
- copy to clipboard service support

- add integration tests support
- add scss linting e.g. stylelint
- add unit tests support
- add e2e tests support
- jira ticket commit linting support https://github.com/Gherciu/commitlint-jira
- gitlab-ci
- github ci
- a11y
